package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";
	
	// 支店
	private static final String BRANCH = "支店";
	
	// 支店定義ファイルの条件
    private static final String BRANCH_CONDITION = "^[0-9]{3}$";
    
	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";
	
	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 商品
	private static final String COMMODITY = "商品";
	
	// 商品定義ファイルの条件
	private static final String COMMODITY_CONDITION = "^[A-Za-z0-9]{8}$";
	
	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";
	
	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String DEFINITIONFILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_SERIALNUMBER = "売上ファイル名が連番になっていません";
	private static final String FILE_INVALID_FORMAT = "のフォーマットが不正です";
	private static final String FILE_INVALID_CODEFORMAT = "の支店コードが不正です";
	private static final String FILE_INVALID_NUMBER = "合計金額が10桁を超えました";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
	    Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
	    Map<String, String> commodityNames = new HashMap<>();
	    // 商品コードと売上金額を保持するMap
	    Map<String, Long> commoditySales = new HashMap<>();

		//コマンドライン引数が渡されているか確認
		if (args.length != 1) {
		    System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, BRANCH, BRANCH_CONDITION)) {
			return;
		}
		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, COMMODITY, COMMODITY_CONDITION)) {
			return;
		}
		
		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		ArrayList<File> rcdFiles = new ArrayList<File>();

		//対象がファイルなのか確認、指定のフォルダから数字8桁。拡張子.rcdをリストに読み込む
		for(int i = 0; i < files.length ; i++) {
		    if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$") ) {
			    rcdFiles.add(files[i]);
		    } 
		}
		
		//売り上げリストが連番か確認
		Collections.sort(rcdFiles);	
		
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
		    int former = Integer.parseInt(files[i].getName().substring(0,8));
		    int latter = Integer.parseInt(files[i + 1].getName().substring(0,8));
			
		    if((latter - former) != 1) {
		        System.out.println(FILE_NOT_SERIALNUMBER);
			    return;
		    }
		}
		
		//売り上げファイルを読み込み処理
		BufferedReader br = null;
		
		//リストの終わりまで繰り返す
		for(int i = 0; i < rcdFiles.size(); i++) {
		    //ファイルを抽出
		    try {
		        FileReader fr = new FileReader(rcdFiles.get(i));
			    br = new BufferedReader(fr);
			    String line;
            
			    //String型を格納するリストを用意
			    ArrayList<String> saleData  = new ArrayList<String>();
			    
			    while((line = br.readLine()) != null) {
			        //リストに要素を追加する
			    	saleData.add(line);
		        }

			     //売上ファイルの中身のフォーマットがあっているか確認
		        if(saleData.size() != 3) {
		            System.out.println(files[i].getName() + FILE_INVALID_FORMAT);
			        return;
		        }
		        
		        //売上金額が数字なのか確認
		        if(!saleData.get(2).matches("^[0-9]+$")) {
		            System.out.println(UNKNOWN_ERROR);
		            return;
		        }
				
		        //売上ファイルの支店コードが支店定義ファイルに存在するか確認
		        if (!branchSales.containsKey(saleData.get(0))) {
		            System.out.println(files[i].getName() + BRANCH + FILE_INVALID_CODEFORMAT);
			        return;
		        }
		        
		        //売上ファイルの支店コードが支店定義ファイルに存在するか確認
		        if (!commoditySales.containsKey(saleData.get(1))) {
		            System.out.println(files[i].getName() + COMMODITY + FILE_INVALID_CODEFORMAT);
			        return;
		        }

		        //集計を始めていく
		        long fileSale = Long.parseLong(saleData.get(2));
		        long saleAmount = (branchSales.get(saleData.get(0))) + fileSale;
		        long commodityAmount = (commoditySales.get(saleData.get(1))) + fileSale;

		        //売上⾦額の合計が10桁を超えたか確認
		        if(saleAmount >= 10000000000L || commodityAmount >= 10000000000L){
		            System.out.println(FILE_INVALID_NUMBER);
			        return;
		        }
				
		        branchSales.put(saleData.get(0), saleAmount);
		        commoditySales.put(saleData.get(1), commodityAmount);
				
		    } catch(IOException e) {
		        System.out.println(UNKNOWN_ERROR);
		        return;
		    } finally {
				// ファイルを開いている場合
		        if(br != null) {
					try {
				        // ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales,String imformation, String condition) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			
			//支店・商品定義ファイルが存在しない場合は、エラーメッセージを表示
			if(!file.exists()) {
				System.out.println(imformation + FILE_NOT_EXIST);
				return false;
			}
			
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			
			// 一行ずつ読み込む
			String line;
			
			while((line = br.readLine()) != null) {
			    // ※ここの読み込み処理を変更してください。(処理内容1-2)
			    String[] items = line.split(",");
				
			    //支店定義ファイルのフォーマットが不正な場合は、エラーメッセージを表示
		        if((items.length != 2) || (!items[0].matches(condition))){
			        System.out.println(imformation + DEFINITIONFILE_INVALID_FORMAT);
			        return false;
			    }
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);
		    }
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			
			for (String key : names.keySet()) {
				String totalSales = Long.toString(sales.get(key));
				bw.write(key + "," + names.get(key) + "," + totalSales);
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}